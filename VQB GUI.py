#VQB Core
#By Fangcat

#Pre Module for GUI
import easygui

try:
    class VQB_Modules:
        def __init__(self):
            print('VQB Core powered by Fangcat')
        def create(self,veradd,vertags,verdescription,verlink):
            DH = open('Versions/'+veradd+'.html','w')
            vt = ''
            for i in vertags:
                vt = vt+'|'+i+'|'
            DH.write('''
            <html>
            <body>
            <h1>%s</h1>
            <p style='color:green;'>%s</p>
            <hr>
            <p>Description:%s</p>
            <hr>
            <a href='%s'><button>Download</button></a>
            '''%(veradd,vt,verdescription,verlink))
            DH.close()

    #VQB GUI

    while True:
        if easygui.buttonbox('Choose a action','VQB GUI',('Add a version page','Exit')) == 'Add a version page':
            info = easygui.multenterbox("Version's informations",'VQB GUI',['Version','Tags(Python list format)','Description','File Link'])
            vqb = VQB_Modules()
            vqb.create(info[0],info[1],info[2],info[3])
            easygui.msgbox('Created file at:Versions/'+info[0],'.html','VQB GUI')
except:
    easygui.exceptionbox()
        