# V Quick Builder

#### 介绍
前端应用版本介绍页面快速生成工具

#### 软件架构
Python


#### 安装教程

1.  下载Python 3.9
2.  Windows系统请先运行目录下的bat文件，其他系统请先安装easygui库
3.  普通用户直接运行VQB_GUI.py文件，更高级的用户或Python界面开发者可以打开VQB.py主文件自定义功能或者自己定制VQB图形界面。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

[作者官网](http://fcnemo.gitee.io)
